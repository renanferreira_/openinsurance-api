// --------------------------------------------------------------------------------------------- //
const scriptName = String("Expeditions - POST - Response");
const scriptOperationAPI = String("[POST] '/open-insurance/v1/expeditions'");
const scriptVersion = String("0.01");
// --------------------------------------------------------------------------------------------- //
/** SCRIPT GLOBAL VARS */

// --------------------------------------------------------------------------------------------- //
/** UTILS */
function mensagemErro(strErrorCode, strErrorMessage, strErrorDetail, helpUrl, statusCode) {
    try {
        var jsonError = {};
        if (strErrorCode) jsonError.errorCode = String(strErrorCode);
        if (strErrorMessage) jsonError.errorMessage = String(strErrorMessage);
        if (strErrorDetail) jsonError.errorDetail = String(strErrorDetail);
        if (helpUrl) jsonError.helpUrl = String(helpUrl);

        $call.response = new com.sensedia.interceptor.externaljar.dto.ApiResponse();
        $call.response.setStatus(statusCode);
        $call.response.getBody().setString(JSON.stringify(jsonError), "utf-8");
        $call.response.setHeader("Content-type", "application/json");
        $call.stopFlow = true;
        $call.decision.setAccept(false);
    } catch (ex) {
        $call.tracer.trace("Exception message => " + ex.message + " <= in line => " + ex.stack);
        $console.debug(">sensedia => Exception", ex);
        throw ex;
    }
}

// --------------------------------------------------------------------------------------------- //
/** FUNCTIONS */
function buildResponse(response) {
    try {
        var returnBody = {};

        returnBody.result = response.estado;
        returnBody.premiumInsurance = response.prima_total;
        returnBody.taxes = response.impuestos;
        returnBody.netPremiumInsurance = response.prima_neta;
        returnBody.businessNumber = response.numero_negocio;
        returnBody.policyNumber = response.numero_poliza;
        returnBody.errors = response.errores;
        returnBody.message = response.mensaje;

        $response.setHeader("Content-Type", "application/json");
        $response.setStatus($call.response.getStatus());
        $response.getBody().setString(JSON.stringify(returnBody), "utf-8");
    } catch (exception) {
        $call.tracer.trace("Exception message => " + exception.message + " <= in line => " + exception.stack);
        $console.debug(">sensedia => Exception", exception);

        mensagemErro("ERR-400", exception.message, exception.stack, "", 400);
        throw exception;
    }
}

// --------------------------------------------------------------------------------------------- //
/** MAIN */
try {
    $call.tracer.trace(">Sensedia debug => Custom javascript: Name [ " + scriptName + " ] version [ " + scriptVersion + " ] - Operation API [ " + scriptOperationAPI + " ].");

    var bResponse = $call.response.getBody().getString("utf-8");
    var objResponse = JSON.parse(bResponse);

    buildResponse(objResponse);

} catch (exception) {
    $call.tracer.trace("Exception message => " + exception.message + " <= in line => " + exception.stack);
    $console.debug(">sensedia => Exception", exception);

    mensagemErro("ERR-400", exception.message, exception.stack, "", 400);
    throw exception;
}