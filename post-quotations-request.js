// --------------------------------------------------------------------------------------------- //
const scriptName = String("Quotations - POST - Request");
const scriptOperationAPI = String("[POST] '/opendata/v1/[expeditions|quotations]'");
const scriptVersion = String("0.01");
// --------------------------------------------------------------------------------------------- //
/** SCRIPT GLOBAL VARS */

// --------------------------------------------------------------------------------------------- //
/** UTILS */
function mensagemErro(strErrorCode, strErrorMessage, strErrorDetail, helpUrl, statusCode) {
    try {
        var jsonError = {};
        if (strErrorCode) jsonError.errorCode = String(strErrorCode);
        if (strErrorMessage) jsonError.errorMessage = String(strErrorMessage);
        if (strErrorDetail) jsonError.errorDetail = String(strErrorDetail);
        if (helpUrl) jsonError.helpUrl = String(helpUrl);

        $call.response = new com.sensedia.interceptor.externaljar.dto.ApiResponse();
        $call.response.setStatus(statusCode);
        $call.response.getBody().setString(JSON.stringify(jsonError), "utf-8");
        $call.response.setHeader("Content-type", "application/json");
        $call.stopFlow = true;
        $call.decision.setAccept(false);
    } catch (ex) {
        $call.tracer.trace("Exception message => " + ex.message + " <= in line => " + ex.stack);
        $console.debug(">sensedia => Exception", ex);
        throw ex;
    }
}

function isEmptyCheck(jsonObject) {
    return jsonObject && Object.keys(jsonObject).length === 0 && jsonObject instanceof Object;
}

// --------------------------------------------------------------------------------------------- //
/** FUNCTIONS */
function getBodyInsured(request) {
    try {
        var returnBody = {};

        returnBody.GRUPO = "1";
        returnBody.primer_nombre_tomador = request.person.givenName;
        returnBody.segundo_nombre_tomador = request.person.middleName;
        returnBody.primer_apellido_tomador = request.person.lastName;
        returnBody.segundo_apellido_tomador = request.person.surname;
        returnBody.genero = request.person.gender;
        returnBody.fecha_nacimiento_tomador = request.person.birthDate;
        returnBody.numero_documento_tomador = request.person.identification.personIdentificationNumber;
        returnBody.tipo_documento_tomador = request.person.identification.typeOfIdentification;
        returnBody.fecha_expedicion_doc = request.person.identification.expeditionDate;
        //returnBody. = request.person.nationality.code;
        returnBody.pais_nacimiento = request.person.nationality.name;
        returnBody.ciudad = request.person.postalAddress.city;

        returnBody.direccion_tomador = String(request.person.postalAddress.streetName) + " " + String(request.person.postalAddress.streetNumber) + " " +
            String(request.person.postalAddress.streetBuildingIdentification) + " " + String(request.person.postalAddress.department);

        returnBody.email = request.person.electronicAddress.emailAddress;
        returnBody.telefono = request.person.phoneAddress.phoneNumber;
        returnBody.celular = request.person.phoneAddress.mobileNumber;
        returnBody.tomador_PEP = (request.isPEP ? "Si" : "No");
        returnBody.tipo_conexion_PEP = request.relationship;

        if (request.isPEP) {
            returnBody.numero_documento_PEP = request.detailPEP.PEP.person.identification.personIdentificationNumber;
            returnBody.tipo_documento_PEP = request.detailPEP.PEP.person.identification.typeOfIdentification;
            //returnBody. = request.detailPEP.PEP.person.identification.expeditionDate;
            returnBody.primer_nombre_PEP = request.detailPEP.PEP.person.givenName;
            returnBody.segundo_nombre_PEP = request.detailPEP.PEP.person.middleName;
            returnBody.primer_apellido_PEP = request.detailPEP.PEP.person.lastName;
            returnBody.segundo_apellido_PEP = request.detailPEP.PEP.person.surname;
            returnBody.parentesco_PEP = request.detailPEP.relationship;
            returnBody.relacion_socio = request.detailPEP.patnership;
            returnBody.porcentaje_participacion = request.participationPercentage;
        }

        return returnBody;
    } catch (exception) {
        $call.tracer.trace("Exception message => " + exception.message + " <= in line => " + exception.stack);
        $console.debug(">sensedia => Exception", exception);

        mensagemErro("ERR-400", exception.message, exception.stack, "", 400);
        throw exception;
    }
}

function getBodyBeneficiary(request) {
    try {
        var returnBody = {};

        returnBody.GRUPO = "2";
        returnBody.primer_nombre_beneficiario = request.beneficiary.person.givenName;
        returnBody.segundo_nombre_beneficiario = request.beneficiary.person.middleName;
        returnBody.primer_apellido_beneficiario = request.beneficiary.person.lastName;
        returnBody.segundo_apellido_beneficiario = request.beneficiary.person.surname;
        returnBody.numero_documento_beneficiario = request.beneficiary.identification.personIdentificationNumber;
        returnBody.tipo_documento_beneficiario = request.beneficiary.identification.typeOfIdentification;
        returnBody.parentesco_beneficiario = request.relationship;
        returnBody.porcentaje_beneficiario = request.percentageBenefit;

        return returnBody;
    } catch (exception) {
        $call.tracer.trace("Exception message => " + exception.message + " <= in line => " + exception.stack);
        $console.debug(">sensedia => Exception", exception);

        mensagemErro("ERR-400", exception.message, exception.stack, "", 400);
        throw exception;
    }
}

function getBodyInsuredPolicy(request) {
    try {
        var returnBody = {};

        returnBody.fecha_inicio_vigencia = request.expeditionDate;
        returnBody.valor_asegurado = request.insuredDeclaredValue;
        returnBody.periodicidad_pago = request.payment;

        return returnBody;
    } catch (exception) {
        $call.tracer.trace("Exception message => " + exception.message + " <= in line => " + exception.stack);
        $console.debug(">sensedia => Exception", exception);

        mensagemErro("ERR-400", exception.message, exception.stack, "", 400);
        throw exception;
    }
}

// --------------------------------------------------------------------------------------------- //
/** MAIN */
try {
    $call.tracer.trace(">Sensedia debug => Custom javascript: Name [ " + scriptName + " ] version [ " + scriptVersion + " ] - Operation API [ " + scriptOperationAPI + " ].");

    var bRequest = $call.request.getBody().getString("utf-8");
    var objRequest = JSON.parse(bRequest);
    var newBody = {};

    newBody.contenido = [];

    isEmptyCheck(objRequest.insured) ? mensagemErro("ERR-400", "Bad request", "Required fields missing - insured", "", 400) : newBody.contenido.push(getBodyInsured(objRequest.insured.insured));

    if(objRequest.beneficiary != null && objRequest.beneficiary != 'null' && objRequest.beneficiary != undefined) {
        if (!(isEmptyCheck(objRequest.beneficiary))) {
            for (let i = 0; i < objRequest.beneficiary.length; i++) {
                newBody.contenido.push(getBodyBeneficiary(objRequest.beneficiary[i]));
            }
        }
    }

    isEmptyCheck(objRequest.insurancePolicyInformation) ? mensagemErro("ERR-400", "Bad request", "Required fields missing - insurancePolicyInformation", "", 400) : newBody.contenido.push(getBodyInsuredPolicy(objRequest.insurancePolicyInformation));
    
    $call.tracer.trace(">Sensedia debug => newBody:    " + JSON.stringify(newBody));

    /* SET Header and Body Request*/
    $call.request.setHeader("Content-Type", "application/json");
    $call.request.setHeader("x-apikey", String($call.environmentVariables.get('opin-personal-accidents-apikey')));
    $call.request.setHeaderCaseSensitive("Authorization", "Basic " + String($call.environmentVariables.get('opin-personal-accidents-authcode')));
    $call.request.setHeader("host", String($call.environmentVariables.get('opin-personal-accidents-host')));

    $request.getBody().setString(JSON.stringify(newBody), "utf-8");

} catch (exception) {
    $call.tracer.trace("Exception message => " + exception.message + " <= in line => " + exception.stack);
    $console.debug(">sensedia => Exception", exception);

    mensagemErro("ERR-400", exception.message, exception.stack, "", 400);
    throw exception;
}